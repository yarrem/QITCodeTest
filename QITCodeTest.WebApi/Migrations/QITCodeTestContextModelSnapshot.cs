﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using QITCodeTest.WebApi.Models;

namespace QITCodeTest.WebApi.Migrations
{
    [DbContext(typeof(QITCodeTestContext))]
    partial class QITCodeTestContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2");

            modelBuilder.Entity("QITCodeTest.Core.Models.LearningClass", b =>
                {
                    b.Property<Guid>("LearningClassId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Location");

                    b.Property<string>("Name");

                    b.Property<string>("Teacher");

                    b.HasKey("LearningClassId");

                    b.ToTable("LearningClasses");
                });

            modelBuilder.Entity("QITCodeTest.Core.Models.Student", b =>
                {
                    b.Property<Guid>("StudentId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateOfBirth");

                    b.Property<float>("GradePointAverage");

                    b.Property<Guid>("LearningClassId");

                    b.Property<string>("Name");

                    b.Property<string>("Surname");

                    b.HasKey("StudentId");

                    b.HasIndex("LearningClassId");

                    b.HasIndex("Surname")
                        .IsUnique();

                    b.ToTable("Students");
                });

            modelBuilder.Entity("QITCodeTest.Core.Models.Student", b =>
                {
                    b.HasOne("QITCodeTest.Core.Models.LearningClass", "LearningClass")
                        .WithMany("Students")
                        .HasForeignKey("LearningClassId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
