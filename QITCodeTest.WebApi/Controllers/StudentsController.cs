using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using QITCodeTest.Core.Models;
using QITCodeTest.WebApi.Repository;
using QITCodeTest.Core.Repository;

namespace QITCodeTest.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class StudentsController : Controller
    {

      private readonly IRepository<Student> _repository;

      public StudentsController(IRepository<Student> repository)
      {
        _repository = repository;
      }

      // GET api/Student/5
      [HttpGet]
      public IEnumerable<Student> Get()
      {
        return _repository.Get();
      }

      // GET api/Student/ofclass/5
      [HttpGet("ofclass/{id}")]
      public IEnumerable<Student> GetStudentsOfClass(Guid id)
      {
        return _repository.Get().Where(st => st.LearningClassId == id);
      }

      // GET api/Student/5
      [HttpGet("{id}")]
      public Student Get(Guid id)
      {
        return _repository.GetById(id);
      }

      // POST api/Student
      [HttpPost]
      public void Post([FromBody]Student student)
      {
        _repository.Insert(student);
        _repository.Save();
      }

      // PUT api/Student/5
      [HttpPut("{id}")]
      public void Put(Guid id, [FromBody]Student student)
      {
        _repository.Update(student);
        _repository.Save();
      }

      // DELETE api/Student/5
      [HttpDelete("{id}")]
      public void Delete(Guid id)
      {
        _repository.Delete(id);
        _repository.Save();
      }
    }
}
