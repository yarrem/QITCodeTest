﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using QITCodeTest.Core.Models;
using QITCodeTest.WebApi.Repository;
using QITCodeTest.Core.Repository;

namespace QITCodeTest.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class LearningClassesController : Controller
    {

      private readonly IRepository<LearningClass> _repository;

      public LearningClassesController(IRepository<LearningClass> repository)
      {
        _repository = repository;
      }

      // GET api/LearningClass/5
      [HttpGet]
      public IEnumerable<LearningClass> Get()
      {
        return _repository.Get();
      }

      // GET api/LearningClass/5
      [HttpGet("{id}")]
      public LearningClass Get(Guid id)
      {
        return _repository.GetById(id);
      }

      // POST api/LearningClass
      [HttpPost]
      public void Post([FromBody]LearningClass learningClass)
      {
        _repository.Insert(learningClass);
        _repository.Save();
      }

      // PUT api/LearningClass/5
      [HttpPut("{id}")]
      public void Put(Guid Guid, [FromBody]LearningClass learningClass)
      {
        _repository.Update(learningClass);
        _repository.Save();
      }

      // DELETE api/LearningClass/5
      [HttpDelete("{id}")]
      public void Delete(Guid id)
      {
        _repository.Delete(id);
        _repository.Save();
      }
    }
}
