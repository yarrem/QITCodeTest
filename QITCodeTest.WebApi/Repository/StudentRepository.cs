using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.EntityFrameworkCore;

using QITCodeTest.Core.Repository;
using QITCodeTest.Core.Models;

using QITCodeTest.WebApi.Models;

namespace QITCodeTest.WebApi.Repository
{
  public class StudentRepository : IRepository<Student>
  {

    private readonly QITCodeTestContext _dbContext;

    public StudentRepository(QITCodeTestContext dbContext)
    {
      _dbContext = dbContext;
    }

    public IEnumerable<Student> Get()
    {
      return _dbContext.Students.AsEnumerable();
    }

    public Student GetById(Guid id)
    {
      return _dbContext.Students.FirstOrDefault(c => c.StudentId == id);
    }

    public void Insert(Student student)
    {
      _dbContext.Students.Add(student);
    }

    public void Delete(Guid id)
    {
      var student = GetById(id);
      _dbContext.Students.Remove(student);
    }

    public void Update(Student student)
    {
      _dbContext.Students.Attach(student);
      _dbContext.Entry(student).State = EntityState.Modified;
    }

    public void Save()
    {
      _dbContext.SaveChanges();
    }
  }
}
