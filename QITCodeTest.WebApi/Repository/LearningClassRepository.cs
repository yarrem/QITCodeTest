using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.EntityFrameworkCore;

using QITCodeTest.Core.Models;
using QITCodeTest.Core.Repository;

using QITCodeTest.WebApi.Models;

namespace QITCodeTest.WebApi.Repository
{
  public class LearningClassRepository : IRepository<LearningClass>
  {

    private readonly QITCodeTestContext _dbContext;

    public LearningClassRepository(QITCodeTestContext dbContext)
    {
      _dbContext = dbContext;
    }

    public IEnumerable<LearningClass> Get()
    {
      return _dbContext.LearningClasses.Include("Students").AsEnumerable();
    }

    public LearningClass GetById(Guid id)
    {
      return _dbContext.LearningClasses.FirstOrDefault(c => c.LearningClassId == id);
    }

    public void Insert(LearningClass learningClass)
    {
      _dbContext.LearningClasses.Add(learningClass);
    }

    public void Delete(Guid id)
    {
      var learningClass = GetById(id);
      _dbContext.LearningClasses.Remove(learningClass);
    }

    public void Update(LearningClass learningClass)
    {
      _dbContext.LearningClasses.Attach(learningClass);
      _dbContext.Entry(learningClass).State = EntityState.Modified;
    }

    public void Save()
    {
      _dbContext.SaveChanges();
    }
  }
}
