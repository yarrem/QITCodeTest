using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using QITCodeTest.Core.Models;

namespace QITCodeTest.WebApi.Models
{
  public class QITCodeTestContext : DbContext
  {
    public QITCodeTestContext(DbContextOptions<QITCodeTestContext> options)
      : base(options)
    {}

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<Student>()
        .HasIndex(s => s.Surname)
        .IsUnique();
    }

    public DbSet<Student> Students { get; set; }
    public DbSet<LearningClass> LearningClasses { get; set; }
  }
}
