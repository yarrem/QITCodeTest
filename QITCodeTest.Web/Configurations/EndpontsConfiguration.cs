namespace QITCodeTest.Web.Configurations
{
  public class EndpointsConfiguration
  {
    public string BaseUrl { get; set; }
    public string LearningClassesActions { get; set; }
    public string StudentsActions { get; set; }
  }
}
