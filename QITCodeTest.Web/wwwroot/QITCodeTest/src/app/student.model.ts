import { Validation } from './validation.model';

export class Student {
  studentId: string;
  name: string;
  surname: string;
  dateOfBirth: number;
  gradePointAverage: number;
  learningClassId:  string;

  constructor(learningClassId: string) {
    this.learningClassId = learningClassId;
  }

  validateSurname(students: Student[]) : Validation {
    const isValid = students.every(student => student.surname !== this.surname);
    return {
      isValid,
      message: isValid ? '' : 'Student with such name already exsists!'
    } as Validation;
  }
};
