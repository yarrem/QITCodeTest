import { Component, Input } from '@angular/core';
import { StudentsService } from './students.service';
import { Student } from './student.model';
import { Validation } from './validation.model';

@Component({
  selector: 'studentForm-component',
  templateUrl: './studentForm.component.html'
})
export class StudentFormComponent {
  @Input() student: Student;
  @Input() title: string;
  @Input() isOpen: boolean = false;
  private onActionExecuted: () => void;
  private validation: Validation = new Validation();

  constructor(private studentsService: StudentsService) {
    this.openForm = this.openForm.bind(this);
    this.closeForm = this.closeForm.bind(this);
  }

  closeForm(): void {
    this.isOpen = false;
  }

  openForm(onActionExecuted): void {
    this.onActionExecuted = onActionExecuted;
    this.isOpen = true;
  }

  save(): void {
    this.studentsService
      .saveStudent(this.student)
      .then(this.onActionExecuted)
      .then(this.closeForm)
      .catch(validation => this.validation = validation)
    ;
  }
}
