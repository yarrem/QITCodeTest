import { Student } from './student.model';

export class LearningClass {
  learningClassId: string;
  name: string;
  location: string;
  teacher: number;
  students: Student[];
}

