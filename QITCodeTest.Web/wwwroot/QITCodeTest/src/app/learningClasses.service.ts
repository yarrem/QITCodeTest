import { Injectable } from '@angular/core';
import { LearningClass } from './learningClass.model';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class LearningClassesService {
  private learningClassesCache: { } = { };

  constructor(private http: Http) { }

  getLearningClasses(): Promise<LearningClass[]> {
    return this.http.get(
      'http://localhost:5050/api/learningClasses'
    )
    .toPromise()
    .then(res => res.json() as LearningClass[])
    .then(learningClasses => {
      this.learningClassesCache = learningClasses.reduce(
        (res, lClass) => ({
          ...res,
          [lClass.learningClassId]: {
            ...lClass,
            students: lClass.students.reduce((res, st) => ({ ...res, [st.studentId]: st }), {})
          }
        })
        , {});
      return learningClasses;
    })
    ;
  }

  saveLearningClass(learningClass: LearningClass): Promise<LearningClass[]> {
    if (learningClass.learningClassId) {
      return this.http.put(
        `http://localhost:5050/api/learningClasses/${learningClass.learningClassId}`,
        learningClass,
        new Headers({'content-type': 'application/json'})
      )
        .toPromise()
        .then(res => this.getLearningClasses())
      ;
    }
    return this.http.post(
      `http://localhost:5050/api/learningClasses/`,
      learningClass,
      new Headers({'content-type': 'application/json'})
    )
    .toPromise()
    .then(res => this.getLearningClasses())
    ;
  }

  deleteLearningClass(learningClass: LearningClass): Promise<LearningClass[]> {
    return this.http.delete(
      `http://localhost:5050/api/learningClasses/${learningClass.learningClassId}`,
      new Headers({'content-type': 'application/json'})
    )
      .toPromise()
      .then(res => this.getLearningClasses())
    ;
  }

  getStudentsOfClassCache(learningClassId) {
    return (this.learningClassesCache[learningClassId] || {}).students;
  }
}
