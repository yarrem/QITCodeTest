import { Injectable } from '@angular/core';
import { Student } from './student.model';
import { Http, Headers } from '@angular/http';
import { LearningClassesService } from './learningClasses.service';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class StudentsService {
  private studentsCache: { } = { };
  private endpoint = `${window['configuration'].baseUrl}${window['configuration'].studentsActions}`;

  constructor(private http: Http, private learningClassesService: LearningClassesService) { }

  getStudents(learningClassId: string): Promise<Student[]> {
    return this.http.get(
      `${this.endpoint}ofclass/${learningClassId}`
    )
    .toPromise()
    .then(res => res.json() as Student[])
    .then(students => {
      this.studentsCache[learningClassId] = students.reduce(
        (res, student) => ({ ...res, [student.studentId]: student })
        , {});
      return students;
    })
    ;
  }

  getSudentsOfClass(learningClassId): Promise<Student[]> {
    return new Promise(resolve => {
      let students = this.studentsCache[learningClassId]
        || this.learningClassesService.getStudentsOfClassCache(learningClassId);

      if (!students) {
        this.getStudents(learningClassId).then((students) => {
          resolve(students);
        });
      }
      resolve(Object.keys(students || {}).map(key => students[key]));
    });
  }

  saveStudent(student: Student) : Promise<Student[]> {
    const studentId = student.studentId;
    const learningClassId = student.learningClassId;
    const stCache = this.studentsCache[learningClassId]
      || this.learningClassesService.getStudentsOfClassCache(learningClassId);
    const currentStudents = Object.keys(stCache || []).map(key => stCache[key]);
    const validation = student.validateSurname(currentStudents);

    if (!validation.isValid) {
      return new Promise((_, reject) => reject(validation));
    }

    if (studentId) {
      return this.http.put(
        `${this.endpoint}${studentId}`,
        student,
        new Headers({'content-type': 'application/json'})
      )
        .toPromise()
        .then(res => this.getStudents(learningClassId))
      ;
    }

    return this.http.post(
      `${this.endpoint}`,
      student,
      new Headers({'content-type': 'application/json'})
    )
    .toPromise()
    .then(res => this.getStudents(learningClassId))
    ;
  }

  deleteStudent(student: Student) : Promise<Student[]> {
    return this.http.delete(
      `${this.endpoint}${student.studentId}`,
      new Headers({'content-type': 'application/json'})
    )
      .toPromise()
      .then(res => this.getStudents(student.learningClassId))
    ;
  }
}
