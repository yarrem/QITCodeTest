import { Component, OnInit, ViewChild } from '@angular/core';
import { NgClass } from '@angular/common';
import { Student } from './student.model';
import { LearningClassesService } from './learningClasses.service';
import { LearningClassFormComponent } from './learningClassForm.component';
import { LearningClass } from './learningClass.model';

@Component({
  selector: 'learningClasses-component',
  templateUrl: './learningClasses.component.html',
})
export class LearningClassesComponent implements OnInit {
  @ViewChild(LearningClassFormComponent) form;
  learningClasses: LearningClass[];
  learningClass: LearningClass = new LearningClass();
  formTitle: string;
  private hasErrors: boolean;
  private message = "Oops somethig happened! please reload the page";

  constructor(private learningClassesService: LearningClassesService) { }

  getLearningClasses(): void {
    this.learningClassesService
      .getLearningClasses()
      .then(classes => this.learningClasses = classes)
      .catch(() => this.hasErrors = true);
    ;
    console.log(this.learningClasses);
  }

  addLearningClass(): void {
    this.formTitle = 'Add new class';
    this.learningClass = new LearningClass();
    this.form.openForm(classes => this.learningClasses = classes);
  }

  ngOnInit(): void {
    this.getLearningClasses()
  }

  editLearningClass(learningClass: LearningClass): void {
    this.formTitle = `Edit ${learningClass.name}`;
    this.learningClass = learningClass;
    this.form.openForm(classes => this.learningClasses = classes);
  }

  deleteLearningClass(learningClass: LearningClass): void {
    this.learningClassesService
      .deleteLearningClass(learningClass)
      .then(classes => this.learningClasses = classes)
      .catch(() => this.hasErrors = true);
    ;
  }
}
