import {
  Component,
  Input,
  ViewChild,
  OnInit
} from '@angular/core';
import { StudentsService } from './students.service';
import { Student } from './student.model';
import { StudentFormComponent } from './studentForm.component';

@Component({
  selector: 'students-component',
  templateUrl: './students.component.html'
})
export class StudentsComponent implements OnInit {
  @ViewChild(StudentFormComponent) form;
  @Input() learningClassId: string;
  formTitle: string;
  @Input() students: Student[];
  student: Student = new Student(this.learningClassId);
  private hasErrors: boolean;
  private message = "Oops somethig happened! please reload the page";

  constructor(private studentsService: StudentsService) { }

  getStudents(): void {
    this.studentsService
      .getSudentsOfClass(this.learningClassId)
      .then(students => this.students = students)
      .catch(() => this.hasErrors = true);
    ;
  }

  ngOnInit() { this.getStudents(); }

  addStudent(student: Student): void {
    this.formTitle = 'Add new class';
    this.student = new Student(this.learningClassId);
    this.form.openForm(students => this.students = students);
  }

  editStudent(student: Student): void {
    this.formTitle = `Edit ${student.name} ${student.surname}`;
    this.student = student;
    this.form.openForm(students => this.students = students);
  }

  deleteStudent(student: Student): void {
    this.studentsService
      .deleteStudent(student)
      .then(students => this.students = students)
      .catch(() => this.hasErrors = true);
    ;
  }
}
