import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { StudentsComponent } from './students.component';
import { LearningClassesComponent } from './learningClasses.component';
import { LearningClassFormComponent } from "./learningClassForm.component";
import { LearningClassesService } from "./learningClasses.service";
import { StudentsService } from "./students.service";
import { StudentFormComponent } from "./studentForm.component";

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentFormComponent,
    LearningClassesComponent,
    LearningClassFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    LearningClassesService,
    StudentsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
