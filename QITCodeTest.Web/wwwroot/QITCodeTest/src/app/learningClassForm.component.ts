import { Component, Input } from '@angular/core';
import { LearningClassesService } from './learningClasses.service';
import { LearningClass } from './learningClass.model';
import { Validation } from './validation.model';

@Component({
  selector: 'learningClassForm-component',
  templateUrl: './learningClassForm.component.html',
})
export class LearningClassFormComponent {
  @Input() learningClass: LearningClass;
  @Input() title: string;
  @Input() isOpen: boolean = false;
  private onActionExecuted: () => void;
  private validation: Validation = new Validation();

  constructor(private learningClassesService: LearningClassesService) { }

  closeForm(): void {
    this.isOpen = false;
  }

  openForm(onActionExecuted): void {
    this.onActionExecuted = onActionExecuted;
    this.isOpen = true;
  }

  save(): void {
    this.learningClassesService
      .saveLearningClass(this.learningClass)
      .then(this.onActionExecuted)
      .then(this.closeForm)
      .catch(validation => this.validation = validation)
    ;
  }
}
