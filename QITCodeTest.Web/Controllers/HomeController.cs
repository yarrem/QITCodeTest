﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using QITCodeTest.Web.Configurations;

namespace QITCodeTest.Web.Controllers
{
    public class HomeController : Controller
    {
      private readonly EndpointsConfiguration _configuration;

      public HomeController(IOptions<EndpointsConfiguration> configuationAccessor)
      {
        _configuration = configuationAccessor.Value;
      }

      public IActionResult Index()
      {
          return View(_configuration);
      }
    }
}
