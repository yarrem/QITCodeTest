This project assumes that you have MySQL server and dotnet core cli isnatlled on your computer.

Update ```DefaultConnection``` string inside QITCodeTest.WebApi/appsettings.json to match your MySQL server client

From the root folder of the solution run:
```
dotnet restore
dotnet build
```

From the folder of QITCodeTest.WebApi run:
```
dotnet ef database update
dotnet run
```

Compile static files inside QITCodeTest.Web/wwwroot/QITCodeTest
(look into this folder for instructionst on how to do it)

From the QITCodeTest.Web run:
```
dotnet run
```

Default routes:
http://localhost:5000 - webpage
http://localhost:5050/api/learningClasses - learningClasses api
http://localhost:5050/api/students - students api

