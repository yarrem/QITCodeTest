using System;
using System.Collections.Generic;

namespace QITCodeTest.Core.Repository
{
  public interface IRepository<T> where T : class
  {
    IEnumerable<T> Get();
    T GetById(Guid Id);
    void Insert(T record);
    void Delete(Guid recordId);
    void Update(T record);
    void Save();
  }
}
