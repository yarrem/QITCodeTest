using System;

namespace QITCodeTest.Core.Models
{
  public class Student
  {
    public Guid StudentId { get; set; }
    public string Name { get; set; }
    public string Surname { get; set; }
    public DateTime DateOfBirth { get; set; }
    public float GradePointAverage { get; set; }

    public Guid LearningClassId { get; set; }
    public LearningClass LearningClass { get; set; }
  }
}
