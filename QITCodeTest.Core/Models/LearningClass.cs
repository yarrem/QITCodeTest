using System;
using System.Collections.Generic;

namespace QITCodeTest.Core.Models
{
  public class LearningClass
  {
    public Guid LearningClassId { get; set; }
    public string Name { get; set; }
    public string Location { get; set; }
    public string Teacher { get; set; }
    public List<Student> Students { get; set; }
  }
}
